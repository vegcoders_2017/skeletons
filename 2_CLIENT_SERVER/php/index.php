<?php
require_once 'db.php';
require_once 'table.php';
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
<?php
	$some_value = filter_input(INPUT_GET, 'some_value', FILTER_SANITIZE_SPECIAL_CHARS);
	$table = new Table();
	if ($some_value) {
		echo 'Some value : ' . $some_value;
		$table->insertValue($some_value);
	} else {
		$some_value = '12345';
	}
	$table->showTable();
?>
<form>
	<input type="text" name="some_value" value="<?php echo $some_value;?>">
	<input type="submit" value="Save"/>
</form>
</body>
</html>