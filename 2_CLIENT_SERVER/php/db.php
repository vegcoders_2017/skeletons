<?php
//https://phpmyadmin.adm.tools
define('DB_HOST', 'vegcoder.mysql.ukraine.com.ua');
define('DB_NAME', 'vegcoder_sk');
define('DB_USER', 'vegcoder_sk');
define('DB_PASS', '9vb2zvaj');

class DB
{
	protected static $_instance;

	private $link;


	static public function getInstance()
	{
		if (null === static::$_instance) {
			static::$_instance = new static();
		}
		return static::$_instance;
	}

	function __construct()
	{
		$this->link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->link) {
			echo 'Ошибка: ' . mysqli_connect_errno() . ' : ' . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		$this->link->set_charset('utf8');
	}

	public static function prepare($query) {
		$db_object = self::getInstance();
		return $db_object->link->prepare($query);
	}

	public static function query($query)
	{
		$db_object = self::getInstance();
		return $db_object->link->query($query);
	}

	public function __destruct()
	{
		mysqli_close($this->link);
	}
}



