System.register(['@angular/core', './db'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, db_1;
    var Table, TableService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (db_1_1) {
                db_1 = db_1_1;
            }],
        execute: function() {
            Table = (function () {
                function Table(some_value) {
                    this.some_value = some_value;
                }
                return Table;
            }());
            exports_1("Table", Table);
            TableService = (function () {
                function TableService(db) {
                    this.db = db;
                }
                TableService.prototype.saveJS = function (table) {
                    return this.db.post('http://localhost:7898', { some_value: table.some_value });
                };
                TableService.prototype.savePHP = function (table) {
                    return this.db.post('http://localhost:7897', { some_value: table.some_value });
                };
                TableService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [db_1.DB])
                ], TableService);
                return TableService;
            }());
            exports_1("TableService", TableService);
        }
    }
});
