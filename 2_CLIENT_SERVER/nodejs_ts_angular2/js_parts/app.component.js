System.register(['@angular/core', './table'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, table_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (table_1_1) {
                table_1 = table_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(tableService) {
                    this.tableService = tableService;
                    this.model = new table_1.Table(null);
                    this.status_text = '';
                }
                AppComponent.prototype.onSubmitPHP = function () {
                    var _this = this;
                    this.status_text = 'saving php';
                    this.tableService.savePHP(this.model).then(function (response) {
                        _this.status_text = response.toString();
                    });
                    return false;
                };
                AppComponent.prototype.onSubmitJS = function () {
                    var _this = this;
                    this.status_text = 'saving js';
                    this.tableService.saveJS(this.model).then(function (response) {
                        _this.status_text = response.toString();
                    });
                    return false;
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: 'app/app.component.html'
                    }), 
                    __metadata('design:paramtypes', [table_1.TableService])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
