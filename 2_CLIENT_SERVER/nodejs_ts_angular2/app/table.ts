import {Injectable, Inject} from '@angular/core';
import { DB }    from './db';
export class Table {
	constructor(public some_value: string) {
	}
}
@Injectable() class TableService {
	constructor(public db:DB) {

	}
	saveJS(table) {
		return this.db.post('http://localhost:7898', {some_value : table.some_value});
	}
	savePHP(table) {
		return this.db.post('http://localhost:7897', {some_value : table.some_value});
	}
}

export {TableService};