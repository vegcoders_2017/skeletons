import {Injectable, Inject} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';


@Injectable() class DB {
	constructor(private http: Http) {

	}

	public post(url: string, data: any) {
		return this.http.post(url, data).toPromise();
	}
}

export {DB};


