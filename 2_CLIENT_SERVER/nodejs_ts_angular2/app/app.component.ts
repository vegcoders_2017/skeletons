import { Component } from '@angular/core';
import {Table, TableService}    from './table';
@Component({
	selector: 'my-app',
	templateUrl: 'app/app.component.html'
})
export class AppComponent {
	model = new Table(null);

	status_text : string = '';
	constructor(public tableService:TableService) {
	}
	onSubmitPHP() {
		this.status_text = 'saving php';
		this.tableService.savePHP(this.model).then(response => {
			this.status_text = response.toString();
		});
		return false;
	}
	onSubmitJS() {
		this.status_text = 'saving js';
		this.tableService.saveJS(this.model).then(response => {
			this.status_text = response.toString();
		});
		return false;
	}

}
