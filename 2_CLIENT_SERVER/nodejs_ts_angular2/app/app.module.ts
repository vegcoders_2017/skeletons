import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }  from './app.component';

import { TableService } from "./table";
import { DB } from  "./db";
import { HttpModule } from '@angular/http';

@NgModule({
	imports:      [ BrowserModule, FormsModule, HttpModule ],
	declarations: [ AppComponent ],
	bootstrap:    [ AppComponent ],
	providers: [
		TableService, DB
	],
})
export class AppModule { }