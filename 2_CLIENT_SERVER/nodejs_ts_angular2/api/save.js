var express = require('express');
var app = express();

// for working with different domains (:3000 angular will give error if remove this)
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	// Pass to next layer of middleware
	next();
});

//init once
var mysql      = require('mysql');
var connection = mysql.createConnection({
	host     : 'vegcoder.mysql.ukraine.com.ua',
	user     : 'vegcoder_sk',
	password : '9vb2zvaj',
	database : 'vegcoder_sk'
});
connection.connect(function(err) {
	if (err) {
		console.log('Unable to connect to MySQL.')
		process.exit(1);
	} else {
		app.listen(7898, function() {
			console.log('Listening on port 7898...')
		})
	}
});

//some default
app.get('/', function (req, res) {
	res.send('hello');
});

//post data processing
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.post('/', function (req, res) {
	console.log('Post processing', req.body);
	if (req.body.some_value) {
		connection.query('INSERT INTO test_table (some_title, crtime) VALUES (?, NOW())', req.body.some_value,
			function (err, result) {
				if (err) throw err;
				console.log('Added id:' + result.insertId);
				res.send('Added id:' + result.insertId);
			}
		);
	}
});