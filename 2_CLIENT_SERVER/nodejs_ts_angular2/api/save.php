<?php
require_once __DIR__ . '/libs/_init.php';

// for working with different domains (:3000 angular will give error if remove this)
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

$data = json_decode(file_get_contents('php://input'));

$some_value = filter_var(@$data->some_value, FILTER_SANITIZE_SPECIAL_CHARS);
$table = new Table();
if ($some_value) {
	echo 'Some value : ' . $some_value;
	$table->insertValue($some_value);
} else {
	echo 'NOTHING';
}