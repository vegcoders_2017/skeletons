(function (global) {
	System.config({
		paths: {
			// paths serve as alias
			'npm:': 'https://unpkg.com/'
		},
		// map tells the System loader where to look for things
		map: {
			// our app is within the app folder
			app: './js_parts/',

			// angular bundles
			'@angular': './node_modules/@angular',

			// angular bundles @todo local
			'@angular/core': 'npm:@angular/core/bundles/core.umd.js',
			'@angular/common': 'npm:@angular/common/bundles/common.umd.js',
			'@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
			'@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
			'@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
			'@angular/http': 'npm:@angular/http/bundles/http.umd.js',
			'@angular/router': 'npm:@angular/router/bundles/router.umd.js',
			'@angular/router/upgrade': 'npm:@angular/router/bundles/router-upgrade.umd.js',
			'@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
			'@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
			'@angular/upgrade/static': 'npm:@angular/upgrade/bundles/upgrade-static.umd.js',

			// other libraries
			'rxjs':                      'npm:rxjs@5.0.1',
			'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
			'ts':                        'npm:plugin-typescript@5.2.7/lib/plugin.js',
			'typescript':                'npm:typescript@2.0.10/lib/typescript.js',

		},
		// packages tells the System loader how to load when no filename and/or no extension
		packages: {
			app: {
				main: 'main.js',
				defaultExtension: 'js'
			},
			rxjs: {
				defaultExtension: 'js'
			}
		}
	});

})(this);