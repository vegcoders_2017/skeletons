<?php
class Table
{
	public function insertValue($some_value)
	{
		$query = 'INSERT INTO test_table (some_title, crtime) VALUES (?, NOW())';
		$stmt = DB::prepare($query);
		$stmt->bind_param('s', $some_value);
		$stmt->execute();
	}

	public function showTable()
	{
		if ($result = DB::query('SELECT * FROM test_table')) {
			echo '<table>';
			while ($row = $result->fetch_array()) {
				echo '<tr><td>' . $row['some_title'] . '</td></tr>';
			}
			echo '</table>';
			$result->close();
		}
	}
}


