<?php
require_once __DIR__ . '/libs/_init.php';

$some_value = filter_input(INPUT_GET, 'some_value', FILTER_SANITIZE_SPECIAL_CHARS);
$table = new Table();
if ($some_value) {
	echo 'Some value : ' . $some_value;
	$table->insertValue($some_value);
} else {
	echo 'NOTHING';
}