<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
</head>
<body>
<div id="my_table"></div>
<form id="my_form">
	<input type="text" name="some_value" value="">
	<input type="submit" value="Save"/>
</form>
<script>
	function loadTable() {
		$.ajax({
			url: './api/load.php',
		}).done(function(data) {
			$('#my_table').html(data);
		})
	}
	$( '#my_form' ).submit(function( event ) {
		$.ajax({
			url: './api/save.php',
			data: $('#my_form').serialize()
		}).done(function(data) {
			loadTable();
		})
		event.preventDefault();
	});
	$( document ).ready(function() {
		loadTable();
	});
</script>
</body>
</html>