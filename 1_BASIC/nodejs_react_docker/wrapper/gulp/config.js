var dest = './build',
	src = './src',
	mui = './node_modules/material-ui/src';

module.exports = {
	browserSync: {
		server: {
			// We're serving the src folder as well
			// for sass sourcemap linking
			baseDir: [dest, src]
		},
		open: false,
		files: [
			dest + '/**'
		],
		port: 8870,
		ui: {
			port: 8871
		}
	},
	markup: {
		src: src + "/www/**",
		dest: dest
	},
	browserify: {
		// Enable source maps
		debug: true,
		// A separate bundle will be generated for each
		// bundle config in the list below
		bundleConfigs: [{
			entries: src + '/app/app.js',
			dest: dest,
			outputName: 'app.js'
		}],
		extensions: ['.js'],
	}
};
