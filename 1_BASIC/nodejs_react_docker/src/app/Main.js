import React, {Component} from 'react';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Badge from 'material-ui/Badge';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';

import {orange500, deepPurple900, deepPurple400, indigo700} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Paper from 'material-ui/Paper';

import CommunicationCall from 'material-ui/svg-icons/communication/call';
import CommunicationForum from 'material-ui/svg-icons/communication/forum';
import ActionInput from 'material-ui/svg-icons/action/input';

import TextField from 'material-ui/TextField';

const styles = {
	title: {
		cursor: 'pointer',
	},
	container: {
		textAlign: 'center',
		paddingTop: 300
	},
	topBar: {
		position: 'fixed'
	},
	topIcons: {
		backgroundColor: 'transparent',
		color: 'white',
		paddingTop:10,
		paddingRight:10
	},
	createDialog: {
		position: 'absolute',
		top:0
	},
};

const muiTheme = getMuiTheme({
	palette: {
		primary1Color: deepPurple900,
		primary2Color: deepPurple400,
		accent1Color: orange500,
		pickerHeaderColor: indigo700,
	},
	appBar: {
		height: 50,
	}
});

class Main extends Component {
	constructor(props, context) {
		super(props, context);

		this.handleCreateClose = this.handleCreateClose.bind(this);
		this.handleCreateTouchTap = this.handleCreateTouchTap.bind(this);

		this.state = {
			open: false,
		};
	}

	handleTitleTouchTap() {
		alert('menu');
	}

	handleLoginTouchTap() {
		alert('login');
	}


	handleCreateTouchTap() {
		this.setState({
			open: true,
		});
	}

	handleCreateClose() {
		this.setState({
			open: false,
		});
	}


	render() {
		const standardActions = (
			<div><FlatButton
				label="Начать позже"
				primary={false}
				onTouchTap={this.handleCreateClose}
			/>
				<RaisedButton
					label="Продолжить"
					primary={true}
					onTouchTap={this.handleCreateProcess}
				/>
			</div>
		);

		const rightButtons = (
			<div>
				<IconButton	onTouchTap={this.handleLoginTouchTap} style={styles.topIcons} tooltip="Вход">
					<ActionInput color="white"/>
				</IconButton>
				<IconButton	onTouchTap={this.handleLoginTouchTap} style={styles.topIcons} tooltip="Позвонить">
					<CommunicationCall color="white"/>
				</IconButton>
				<IconButton	onTouchTap={this.handleLoginTouchTap} style={styles.topIcons} tooltip="База знаний">
					<CommunicationForum color="white"/>
				</IconButton>
			</div>
		);

		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				<div>

					<AppBar
						style={styles.topBar}
						title={<span style={styles.title}>Тестовый Сайт</span>}
						onTitleTouchTap={this.handleTitleTouchTap}
						iconElementLeft={<IconButton onTouchTap={this.handleTitleTouchTap} tooltip="Меню"><NavigationMenu /></IconButton>}
						iconElementRight={rightButtons}
					/>

					<Dialog
						open={this.state.open}
						title="Регистрация"
						actions={standardActions}
						style={styles.createDialog}
					>
						<p>
							Для доступа к сервисам пройдите регистрацию - это не займет много времени.
						</p>
						<TextField
							autoFocus={true}
							hintText="Ваш электронный адрес или мобильный телефон"
							floatingLabelText="Email / Телефон"
							type="text"
							fullWidth={true}
						/>
						<TextField
							hintText="Ваш пароль (не менее 6 символов)"
							floatingLabelText="Пароль"
							type="password"
							fullWidth={true}
						/>

						<p>
							Нажав кнопку "Продолжить" Вы соглашаетесь с правилами и условиями использования.
						</p>
					</Dialog>

					<div style={styles.container}>
						<RaisedButton
							label="Начать111222"
							secondary={true}
							onTouchTap={this.handleCreateTouchTap}
						/>
					</div>

				</div>
			</MuiThemeProvider>
		);
	}
}

export default Main;